$(document).ready(function() {
  	 $('.t_group .cal_popup').on('click', function(){
  	 	 $(this).toggleClass('active');
  	 });
 
     $('input[type="tel"]').mask('+7(999) 999-99-99');

  	  $('.t_group button').on('click', function(){
    	 	$(this).toggleClass('active');
  	 })
      $('body').click(function(e){
          if($('a.hamburger').hasClass('active')){
              if(!$(e.target).is('.mob_header, .mob_header *, a.hamburger, a.hamburger *, .dropdown-backdrop, .dropdown-backdrop *')) {
                  $('body').removeClass('active');
                    $('a.hamburger').removeClass('active');
              }
          }
      }); 

      $('.form_group.parol a').on('click', function(e){
      e.preventDefault();
      $(this).toggleClass('active');
      if($(this).hasClass('active')){
        $(this).prev().attr('type', 'text'); 
      }
      else{
        $(this).prev().attr('type', 'password'); 
      }
    })
      
      $('a.hamburger').on('click', function(){
        $('body').addClass('active');
        $(this).addClass('active');
      });

      $('a#vernut').on('click', function(e){
        e.preventDefault();
              if ($(this).prev().attr("disabled")) {
                  $(this).prev().removeAttr("disabled");
                  $(this).text('Вернуть');
              } else {
                  $(this).prev().attr("disabled", "disabled");
                  $(this).text('Изменить');
              }
      })

      $('.jivo_block.active .jivos_main_btn').on('click', function(e){
        $(this).parent().removeClass('active');
        $(this).text('Настроить');
      })

      $('.popup_style a').on('click', function(){
        $(this).parent().find('button.fancybox-button.fancybox-close-small').trigger('click');
      })
      $('.popup_style .btn_link a').on('click', function(){
        $(this).parent().parent().find('button.fancybox-button.fancybox-close-small').trigger('click');
      })

      $('.three_dot>a').on('click', function(){
        $(this).next().slideToggle();
      })

      $('.radios label:nth-child(2)').on('click', function(){
        $(this).parent().addClass('no-checked').removeClass('checked');
      })

      $('.radios label:nth-child(4)').on('click', function(){
        $(this).parent().addClass('checked').removeClass('no-checked');
      })

      $('a#open_pay').on('click', function(){
        $('div#space_pay').slideDown();
        $(this).css('display', 'none');
        $('div#space_pay+hr').css('display', 'none');
      })

     $('a#close-pay_t').on('click', function(){
        $('div#space_pay').slideUp();
        $('a#open_pay').css('display', 'block');
        $('div#space_pay+hr').css('display', 'block');
      })

     $('td.drop_table button').on('click', function(){
      $(this).next().slideToggle();
     })

     $('.delete_img, .delete_icon').on('click', function(e){
        e.preventDefault();
        $(this).parent().remove();
     })

     $('.form_group_first.libr input').on('change', function(){
        if($('.libr input:nth-child(1)').is(':checked')){
            $('.libr2.form_group').hide();
            $('.libr1.form_group').show();
        }
         else{
            $('.libr1.form_group').hide();
            $('.libr2.form_group').show();
        }
     })

     $('#show_all_tovar').on('click', function(e){
        e.preventDefault();
        $('.main_sl_up').slideToggle();
     })

     $('.open_name_variant').on('click', function(){
        $(this).parent().parent().find('.body_name_variant').slideToggle();
     })

     $('a.remove_name_variant').on('click', function(e){
        e.preventDefault();
        $(this).parent().parent().remove();
     })
     
     $('.color_select_radio input').each(function(){
        var r_col = $(this).val();
        $(this).next().css('background-color',r_col);
     })

     $('.set_tab').on('click', function(e){
        e.preventDefault();
        $('.el_right').toggleClass('hides');
        $('.el_left').toggleClass('all_width');
     })

    $('.tit_ad').on('click', function(){
      $(this).next().slideToggle();
      $(this).toggleClass('active');
    })

    $('a.op_faq').on('click', function(e){
      e.preventDefault();
      $('div.op_faq').addClass('active');
    })

    $('a.op_text').on('click', function(e){
      e.preventDefault();
      $('div.op_text').addClass('active');
    })

    $('a.op_ssilk').on('click', function(e){
      e.preventDefault();
      $('div.op_ssilk').addClass('active');
    })

    $('a.op_razdelitel').on('click', function(e){
      e.preventDefault();
      $('div.op_razdelitel').addClass('active');
    })
    $('a.op_html').on('click', function(e){
      e.preventDefault();
      $('div.op_html').addClass('active');
    })
    $('a.op_avatar').on('click', function(e){
      e.preventDefault();
      $('div.op_avatar').addClass('active');
    })
    $('a.op_karta').on('click', function(e){
      e.preventDefault();
      $('div.op_karta').addClass('active');
    })
    $('a.op_sot_seti').on('click', function(e){
      e.preventDefault();
      $('div.op_sot_seti').addClass('active');
    })
    $('a.op_video').on('click', function(e){
      e.preventDefault();
      $('div.op_video').addClass('active');
    })
    $('a.op_mess').on('click', function(e){
      e.preventDefault();
      $('div.op_mess').addClass('active');
    })
     $('a.op_carousel').on('click', function(e){
      e.preventDefault();
      $('div.op_carousel').addClass('active');
    })
      $('a.op_menu').on('click', function(e){
      e.preventDefault();
      $('div.op_menu').addClass('active');
    })
    $('a.op_forma').on('click', function(e){
      e.preventDefault();
      $('div.op_forma').addClass('active');
    })
    $('a.op_predl').on('click', function(e){
      e.preventDefault();
      $('div.op_predl').addClass('active');
    })
    $('a.op_stranit').on('click', function(e){
      e.preventDefault();
      $('div.op_stranit').addClass('active');
    })
    $('a.op_bis_svyazi').on('click', function(e){
      e.preventDefault();
      $('div.op_bis_svyazi').addClass('active');
    })
    $('a.op_uslugi').on('click', function(e){
      e.preventDefault();
      $('div.op_uslugi').addClass('active');
    })
    $('a.op_tovari').on('click', function(e){
      e.preventDefault();
      $('div.op_tovari').addClass('active');
    })
     $('a.op_prei').on('click', function(e){
      e.preventDefault();
      $('div.op_prei').addClass('active');
    })

   $('a.setting_l').on('click', function(e){
    e.preventDefault();
    // $('.el_right').addClass('active');
    $('.homes_f').addClass('active');


    $('.bit_ko').trigger('click');
   })

    $('a.ad_h, .r_act').on('click', function(e){
      e.preventDefault();
      $('.it_one, .it_two').toggleClass('active');
    })

    if ($(window).width() < 960) {
       $('.header_el a.back_l').html('<i class="fas fa-th-large"></i> К страницам');
    }
    if ($(window).width() < 960) {
      var adf = $('div#to_el_fer').clone(true);
      $('.structure_page').html(adf);
      $('.structure_page div#to_el_fer').removeClass('tab')
    }

    $('.rem_act').on('click', function(){
      $(this).parent().removeClass('active');
    })
    $('a.o_prei_after').on('click', function(e){
      e.preventDefault();
    })
    $('.tab_btn_btm a.nazad_or').on('click', function(e){
      e.preventDefault();
      $(this).parent().parent().removeClass('active');
    })
    $('.pos_text_set a').on('click', function(){
      $(this).parent().find('a').removeClass('active');
      $(this).addClass('active');
    })



$('.my_carousel').owlCarousel({
    items:1,
    loop:true,
    center: true,
    // autoWdith: true,
    stagePadding: 40,
    dots: true,
    margin: 8
});

$('.tov_carosuel').owlCarousel({
    items:1,
    loop:true,
    center: true,
    // autoWdith: true,
    stagePadding: 40,
    dots: true,
    margin: 24
});

$('.uslugi_carosuel').owlCarousel({
    items:1,
    loop:true,
    center: true,
    // autoWdith: true,
    stagePadding: 40,
    dots: true,
    margin: 24
});

  if ($(window).width() < 767) {
      $('.homes_f').removeClass('active');
    }

  $('.color_pick input').on('change', function(){
    var etet = $(this).val();
    console.log(etet);
    $(this).next().text(etet);
  })

  $('.menu_top_def a').on('click', function(e){
    e.preventDefault();
    $(this).find('svg').toggleClass('fa-bars fa-times');
    $(this).parent().next().slideToggle();
    $('.menu_def').toggleClass('active');
  })

/********************************ADMIN-MENU***************************************/

  $('.registr_bl li a').on('click', function(e){
      e.preventDefault();
      var ind_li = $(this).parent().index();
      if(!$(this).parent().hasClass('active')){
        $('.main_tab>div').removeClass('active');
        $('.registr_bl li').removeClass('active');
        $(this).parent().addClass('active');
        $('.main_tab>.tabcontent').eq(ind_li).addClass('active');
        $('.panel_left').addClass('active');
      }
      else{
         if ($(window).width() < 1199) {
              $(this).parent().removeClass('active');
              $('.panel_left').removeClass('active');
              $('.main_tab>.tabcontent').eq(ind_li).removeClass('active');
            }
      }
  })

   if ($(window).width() < 1199) {
      $('body').on('click', function(e){
        // $('.panel_left').addClass('active');
        if(!($(e.target).is('.panel_left>.tab, .panel_left>.tab *'))){
          $('.panel_left .registr_bl li').removeClass('active');
          $('.panel_left').removeClass('active');
          // console.log(1);
        }
      })

    }
   if ($(window).width() < 767) {
        $('.panel_left').addClass('open_adm');

   }
    $('.hamburger').on('click', function(){
        $('.panel_left').toggleClass('open_adm');
    })

    $('.op_lib_body').on('click', function(e){
      e.preventDefault();
      $(this).toggleClass('active');
      $(this).parent().parent().next().slideToggle();
    })


    $('.counter_buk input, .counter_buk textarea').on('keyup', function() {
      var xer = $(this).val().length;
      var max_her = parseInt($(this).parent().find('.for_top_count span.count_limit').text(), 10);
      if(xer<max_her+1){
        $(this).parent().find('.for_top_count span.count_yozil').text(xer);
      }
      else{
        $(this).val($(this).val().slice(0, max_her+1));
      }
    })
/********************************ADMIN-MENU***************************************/
/*****************************************SVG SVG SVG******************************************************/

        jQuery('img.svg_hov_blue').each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
                // Get the SVG tag, ignore the rest
                var $svg = jQuery(data).find('svg');

                // Add replaced image ID to the new SVG
                if(typeof imgID !== 'undefined') {
                    $svg = $svg.attr('id', imgID);
                }
                // Add replaced image classes to the new SVG
                if(typeof imgClass !== 'undefined') {
                    $svg = $svg.attr('class', imgClass+' replaced-svg');
                }

                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');

                // Replace image with new SVG
                $img.replaceWith($svg);

            }, 'xml');

        });
/*****************************************SVG SVG SVG******************************************************/

  });
 
 


  